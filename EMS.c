#include <LiquidCrystal.h>
#include <SoftwareSerial.h> //is necesary for the library!! 
SoftwareSerial SIM900A(10, 11);
LiquidCrystal lcd(2, 3, 4, 5, 6, 7);
#include <EEPROM.h>
#include <Time.h>
#include <TimeLib.h>
time_t pTime; //declaring ptime using the time.h library
String _buffer; //declaring buffer as a string variable
int _timeout , address = 0, AC_Voltage1, timecount = 0, AmpsVal = 0, mVperAmp = 125; // declaring variavle types and initializing some
char str[20]; //declaring an array of 20 string elements
float avgV = 0.00 , KWh2, senseADC = 0.00, sampleVal = 0.00, KWhval=0.00, KWhvalue; //declaring all the floating variable and initializing them
int temp, i = 0,  led = 13, count, count1, tSec = 0, KW, KWh; // declaring int variables
double Voltage = 0, VRMS = 0, AmpsRMS = 0; //declaring double variables
char* text;
char* number;
bool error;
char strvolt[] = "Voltage available from MAINS: 000V"; //creating an array of strings and naming them as strvolt
char strkwh[] = "Energy use in KWh: 0.000"; //creating an array of strings and naming them as strkwh
char Row0[20] = "Volt:000  Amp:0.000"; //row0 can only have maximum of 20 alphabets hence we initialize it hence we initialize it value as ome sert of strings
char Row1[20] = "KW:0.000  KWh:0.000"; //row1 can only have maximum of 20 alphabets hence we initialize it hence we initialize it value as ome sert of strings

#define MN1 8
#define GEN 9
#define D0 11
#define D1 12
#define D2 A3
#define D3 A2

void setup() {
  Serial.begin(9600);
  SIM900A.begin(9600);
  delay(1000);
  pinMode(MN1, OUTPUT); //declaring the digital output pins, renaming them and initializing tj=hem as low
  digitalWrite(MN1, LOW);
  pinMode(GEN, OUTPUT);
  digitalWrite(GEN, LOW);
  pinMode(D1, OUTPUT);
  digitalWrite(D1, LOW);
  pinMode(D2, OUTPUT);
  digitalWrite(D2, LOW);
  pinMode(D0, OUTPUT);
  digitalWrite(D0, LOW);
  pinMode(D3, OUTPUT);
  digitalWrite(D3, LOW);
  delay(1000);
  lcd.begin(20, 4);
  lcd.clear();
  lcd.setCursor(0, 0); //setting the cursor to column0 row 0
  lcd.print(" ENERGY  MANAGEMENT");//printing energy management on the 0 row
  lcd.setCursor(0, 1);//set cursor to column 0 row1
  lcd.print( Row0);// printing the  values of Row0 i.e current, voltage, power and energy
  lcd.setCursor(0, 2); //setting the cursor to column 0 row 2
  lcd.print( Row1); //printing the  values of Row0 i. c power and energy
  delay(2000);
  SIM900A.println("AT+CNMI=2,2,0,0,0\r");
  delay(500);
  SIM900A.println("AT+CMGF=1\r");
  delay(1000);
  float valKwh = 0.00f;
  KWhvalue = EEPROM.get(address, valKwh);
  KWhval = 0.000;
  text = strvolt; //text for the message.
  number = "+2348166022246"; //change to a valid number.

}
void loop()
{

  pTime = now();
  senseVolt();
  if (AC_Voltage1 >= 180)
  {
    digitalWrite(GEN, LOW);
    digitalWrite(MN1, HIGH);
    lcd.setCursor(0, 3);
    lcd.print("Power source: MAIN");
    dataget();
    count++;
    if (count == 1)
    {
      Serial.print(text);
      delay(500);
      error = sendmsg(number, text);
      count1 = 0;
    }
    serialEvent();
    if (temp == 1)
    {
      check();

      temp = 0;
      i = 0;
      delay(1000);
    }

  }
  else {
    lcd.setCursor(0, 3);
    lcd.print("Power source: GEN   ");
    count1++;
    if (count1 == 1)
    {
      digitalWrite(MN1, LOW);
      digitalWrite(GEN, HIGH);
      Serial.print(text);
      delay(500);
      error = sendmsg(number, text);
      count = 0;
    }
  }
}

void serialEvent()
{
  if (SIM900A.available())
  {
    Serial.print("waiting for msg");
    if (SIM900A.find("#A."))
    {
      digitalWrite(led, HIGH);
      delay(1000);
      digitalWrite(led, LOW);
      while (SIM900A.available())
      {
        char inChar = SIM900A.read();
        str[i++] = inChar;
        Serial.println(str);
        SIM900A.println("AT+CMGD=1,4");
        if (inChar == '*')
        {
          temp = 1;
          return;
        }
      }
    }
  }
}

bool sendmsg(char* number, char* text)
{
  SIM900A.print (F("AT+CMGF=1\r")); //set sms to text mode
  _buffer = readSerial();
  SIM900A.print (F("AT+CMGS=\""));  // command to send sms
  SIM900A.print (number);
  SIM900A.print(F("\"\r"));
  _buffer = readSerial();
  SIM900A.print (text);
  SIM900A.print ("\r");
  delay(100);
  SIM900A.print((char)26);
  _buffer = readSerial();
  //expect CMGS:xxx   , where xxx is a number,for the sending sms.
  if (((_buffer.indexOf("CMGS") ) != -1 ) ) {
    return true;
  }
  else {
    return false;
  }
}

String readSerial()
{
  _timeout = 0;
  while  (!SIM900A.available() && _timeout < 12000  )
  {
    delay(13);
    _timeout++;
  }
  if (SIM900A.available()) {
    return SIM900A.readString();
  }

}


void senseVolt() {

  sampleVal = 0;
  for (int x = 0; x < 10; x++)
  {
    senseADC = (float)analogRead(A0);
    sampleVal = sampleVal + senseADC; // add samples together
  }
  delay (100);
  avgV =   sampleVal / 10 ;
  AC_Voltage1 =  avgV * 0.005 * 45;
  Row0[5] = (AC_Voltage1 / 100) + 48;
  Row0[6] = ((AC_Voltage1 % 100) / 10) + 48;
  Row0[7] = ((AC_Voltage1 % 100) % 10) + 48;
  lcd.setCursor(0, 1);
  lcd.print( Row0);
  strvolt[30] = Row0[5];
  strvolt[31] = Row0[6];
  strvolt[32] = Row0[7];
  //  Serial.print("Volt:");
  //  Serial.print(AC_Voltage1);
  //  Serial.println(" ");
}

void check()
{
  if (!(strncmp(str, "tv on", 5)))
  {
    digitalWrite(D1, HIGH);
    Serial.println("TV ON");
  }
  else if (!(strncmp(str, "tv off", 6)))
  {
    digitalWrite(D1, LOW);
    Serial.println("TV OFF");
  }
  else if (!(strncmp(str, "fan on", 6)))
  {
    digitalWrite(D2, HIGH);
    Serial.println("FAN ON");
  }
  else if (!(strncmp(str, "fan off", 7)))
  {
    digitalWrite(D2, LOW);
    Serial.println("FAN ON");

  }
  else if (!(strncmp(str, "light on", 8)))
  {
    digitalWrite(D0, HIGH);
    Serial.println("LIGHT ON");
  }
  else if (!(strncmp(str, "light off", 9)))
  {
    digitalWrite(D0, LOW);
    Serial.println("LIGHT OFF");
  }
  else if (!(strncmp(str, "pump on", 7)))
  {
    digitalWrite(D3, HIGH);
    Serial.println("PUMP ON");
  }
  else if (!(strncmp(str, "pump off", 8)))
  {
    digitalWrite(D2, LOW);
    Serial.println("PUMP ON");

  }
  else if (!(strncmp(str, "getKWH", 6)))
  {
    sendmsg("+2347065384842",  strkwh);
  }
}


float getVPP()
{
  float result;

  int readValue;             //value read from the sensor
  int maxValue = 0;          // store max value here
  int minValue = 1024;          // store min value here

  uint32_t start_time = millis();
  while ((millis() - start_time) < 1000) //sample for 1 Sec
  {
    readValue = analogRead(A1);
    // see if you have a new maxValue
    if (readValue > maxValue)
    {
      /*record the maximum sensor value*/
      maxValue = readValue;
    }
    if (readValue < minValue)
    {
      /*record the maximum sensor value*/
      minValue = readValue;
    }
  }

  // Subtract min from max
  result = ((maxValue - minValue) * 5.0) / 1024.0;

  return result;
}


void dataget() {


  Voltage = getVPP();
  VRMS = (Voltage / 2.0) * 0.707;
  AmpsRMS = (VRMS * 1000) / mVperAmp;
  AmpsVal = AmpsRMS * 1000;
  Row0[14] = (AmpsVal / 1000) + 48;
  Row0[16] = ((AmpsVal / 100)) + 48;
  Row0[17] = ((AmpsVal % 100) / 10) + 48;
  Row0[18] = ((AmpsVal % 100) % 10) + 48;
  lcd.setCursor(0, 1);
  lcd.print("            ");
  lcd.setCursor(0, 1);
  lcd.print( Row0);
  Serial.print("Current:");
  Serial.print( AmpsVal);
  Serial.print(" ");

  KW = ( AC_Voltage1 * AmpsRMS);
  Row1[3] = (KW / 1000) + 48;
  Row1[5] = (KW / 100) + 48;
  Row1[6] = ((KW % 100) / 10) + 48;
  Row1[7] = ((KW % 100) % 10) + 48;
  lcd.setCursor(0, 2);
  lcd.print( Row1);
  Serial.print("Kilowatt:");
  Serial.print(KW);
  Serial.print(" ");


  if (AmpsVal > 0) {
    tSec = second(pTime);
    Serial.println(tSec);
    KWhval = KWhval + ((KW * tSec) / 3600);
    Serial.println(KWhval);
    KWh = KWhval;
    Row1[14] = (KWh / 1000) + 48;
    Row1[16] = (KWh / 100)  + 48;
    Row1[17] = ((KWh % 100) / 10) + 48;
    Row1[18] = ((KWh % 100) % 10) + 48;

    strkwh[19] = Row1[14];
    strkwh[21] = Row1[16];
    strkwh[22] = Row1[17];
    strkwh[23] = Row1[18];
    lcd.setCursor(0, 2);
    lcd.print( Row1);
    EEPROM.update(address,   KWhval);
    address = address + 1;
    if (address == EEPROM.length()) {
      address = 0;
    }
    delay(100);
    Serial.print("Kilowatthr:");
    Serial.println(KWh);
  }
  else
  {
    tSec = 0;
    KWh = 0.000;
    Row1[14] = (KWh / 1000) + 48;
    Row1[16] = (KWh / 100)  + 48;
    Row1[17] = ((KWh % 100) / 10) + 48;
    Row1[18] = ((KWh % 100) % 10) + 48;
    lcd.setCursor(0, 2);
    lcd.print( Row1);
    Serial.print("Kilowatthr:");
    Serial.println(KWh);
  }

}




